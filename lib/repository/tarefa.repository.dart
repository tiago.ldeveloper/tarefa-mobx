import 'dart:io';
import 'package:dio/dio.dart';
import 'package:tarefa/models/tarefa.dart';

class TarefaRepository {

  static const String token = "token";
  static const String url = "http://192.168.0.202:8080/api/";

  Future<List<Tarefa>> all() async {
    Response response = await Dio().get(url, options: _options());
    return (response.data as List)
        .map((tarefa) => Tarefa.toJson(tarefa))
        .toList();
  }

  Future<Tarefa> adiciona(Tarefa tarefa) async {
    try {
      Response response = await Dio().post(url, data: tarefa, options: _options());
      return Tarefa.toJson(response.data);
    } catch (e) {
      print("Erro ao adicionar => "+ e.toString());
    }
  }

  Future<void> marcaConcluida(int tarefaId) async{
    String urls = url + tarefaId.toString();
    try{
        print(urls);
       Response response = await Dio().put(urls, options: _options());
    }catch(e){
      print(e);
    }
  }


  Options _options() {
    return Options(
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${token}'},
    );
  }
}
