import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tarefa/controller/tarefa.controller.dart';
import 'package:tarefa/pages/tarefas.dart';

void main() => runApp(TarefaApp());

class TarefaApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => TarefaController()),
      ],
      child:  MaterialApp(
        title: 'Home',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.deepPurpleAccent,
          scaffoldBackgroundColor: Colors.deepPurpleAccent,
          cursorColor: Colors.deepPurpleAccent,
        ),
        home: TarefasPage(),
      ),
    );
  }
}