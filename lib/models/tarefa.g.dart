// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tarefa.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Tarefa on _Tarefa, Store {
  final _$concluidaAtom = Atom(name: '_Tarefa.concluida');

  @override
  bool get concluida {
    _$concluidaAtom.reportRead();
    return super.concluida;
  }

  @override
  set concluida(bool value) {
    _$concluidaAtom.reportWrite(value, super.concluida, () {
      super.concluida = value;
    });
  }

  final _$_TarefaActionController = ActionController(name: '_Tarefa');

  @override
  void toggleConcluida() {
    final _$actionInfo =
        _$_TarefaActionController.startAction(name: '_Tarefa.toggleConcluida');
    try {
      return super.toggleConcluida();
    } finally {
      _$_TarefaActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
concluida: ${concluida}
    ''';
  }
}
