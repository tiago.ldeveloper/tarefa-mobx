
import 'package:mobx/mobx.dart';

part 'tarefa.g.dart';

class Tarefa = _Tarefa with _$Tarefa;

abstract class _Tarefa with Store {

  int id;
  String titulo;

  @observable
  bool concluida = false;

  @action
  void toggleConcluida() => concluida = !concluida;

  _Tarefa({this.titulo});




  _Tarefa.toJson(Map<String, dynamic> json){
    id = json['id'];
    titulo = json['titulo'];
    concluida = json['concluida'];
  }

  Map<String, dynamic>  toJson(){
    final Map<String, dynamic> tarefas = new Map<String, dynamic>();
    tarefas['id'] = this.id;
    tarefas['titulo'] = this.titulo;
    tarefas['concluida'] = this.concluida;

    return tarefas;
  }
}