
import 'package:mobx/mobx.dart';
import 'package:tarefa/models/tarefa.dart';
import 'package:tarefa/repository/tarefa.repository.dart';

part 'tarefa.controller.g.dart';

class TarefaController  = _TarefaController with _$TarefaController;

abstract class _TarefaController with Store {

  final TarefaRepository _repository = TarefaRepository();

  ObservableList<Tarefa> tarefas = ObservableList<Tarefa>();

  @observable
  String titulo = "";

  @action
  void setTitulo(String value) => titulo = value;

  @computed
  bool get isTarefa => titulo.isNotEmpty;

  @action
  Future<void> add() async{
    if(titulo.isNotEmpty){
      var tarefa = await _repository.adiciona(new Tarefa(titulo: titulo));
      this.tarefas.insert(0, tarefa);
      titulo = "";
    }
  }

  @action
  Future<void> check(Tarefa tarefa) async{
     await _repository.marcaConcluida(tarefa.id).then((_) => tarefa.toggleConcluida());
  }

  @action
  Future<void> all() async{
    this.tarefas.clear();
    await _repository.all().then((ltarefas) => tarefas.addAll(ltarefas));
  }
}