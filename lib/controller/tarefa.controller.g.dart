// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tarefa.controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TarefaController on _TarefaController, Store {
  Computed<bool> _$isTarefaComputed;

  @override
  bool get isTarefa =>
      (_$isTarefaComputed ??= Computed<bool>(() => super.isTarefa,
              name: '_TarefaController.isTarefa'))
          .value;

  final _$tituloAtom = Atom(name: '_TarefaController.titulo');

  @override
  String get titulo {
    _$tituloAtom.reportRead();
    return super.titulo;
  }

  @override
  set titulo(String value) {
    _$tituloAtom.reportWrite(value, super.titulo, () {
      super.titulo = value;
    });
  }

  final _$addAsyncAction = AsyncAction('_TarefaController.add');

  @override
  Future<void> add() {
    return _$addAsyncAction.run(() => super.add());
  }

  final _$checkAsyncAction = AsyncAction('_TarefaController.check');

  @override
  Future<void> check(Tarefa tarefa) {
    return _$checkAsyncAction.run(() => super.check(tarefa));
  }

  final _$allAsyncAction = AsyncAction('_TarefaController.all');

  @override
  Future<void> all() {
    return _$allAsyncAction.run(() => super.all());
  }

  final _$_TarefaControllerActionController =
      ActionController(name: '_TarefaController');

  @override
  void setTitulo(String value) {
    final _$actionInfo = _$_TarefaControllerActionController.startAction(
        name: '_TarefaController.setTitulo');
    try {
      return super.setTitulo(value);
    } finally {
      _$_TarefaControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
titulo: ${titulo},
isTarefa: ${isTarefa}
    ''';
  }
}
