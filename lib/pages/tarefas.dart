import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:tarefa/controller/tarefa.controller.dart';
import 'package:tarefa/models/tarefa.dart';

class TarefasPage extends StatefulWidget {


  @override
  _TarefasPageState createState() => _TarefasPageState();
}

class _TarefasPageState extends State<TarefasPage> {

  TextEditingController _controller = TextEditingController();

  String titulo = "";

  @override
  Widget build(BuildContext context) {
    var store = Provider.of<TarefaController>(context);

    store.all();

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          margin: EdgeInsets.fromLTRB(32, 0, 32, 32),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Tarefas', style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Colors.white,
                      fontSize: 28,
                      fontWeight: FontWeight.w900,
                    ),),
                    IconButton(
                      color: Colors.white,
                      icon: Icon(Icons.exit_to_app, color: Colors.white),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10,),

              Expanded(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  color: Colors.white,
                  elevation: 15,

                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 16),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(32),
                          ),
                          child: Observer(
                            builder: (context) {
                              return TextField(
                                controller: _controller,
                                onChanged: store.setTitulo,
                                decoration: InputDecoration(
                                  hintText: 'Tarefas',
                                  border: InputBorder.none,
                                  suffixIcon: store.isTarefa ? Icon(Icons.add) :null,
                                ),
                                textAlignVertical: TextAlignVertical.center,
                                onTap: () {
                                    store.add();
                                    _controller.clear();
                                },
                              );
                            },
                          )
                        ),

                  Observer(
                    builder: (context) {
                      return Expanded(
                        child: ListView.separated(
                          itemCount: store.tarefas.length,
                          itemBuilder: (_, index) {
                            Tarefa tarefa = store.tarefas[index];
                            return Observer(
                              builder: (context) {
                                return ListTile(
                                  title: Text(tarefa.titulo, style: TextStyle(
                                    decoration: tarefa.concluida ? TextDecoration.lineThrough : null,
                                    color: tarefa.concluida ? Colors.grey : Colors.black,
                                  ),),
                                  onTap: () {
                                    store.check(tarefa);
                                  },
                                );
                              },
                            );
                          },
                          separatorBuilder: (_, __) {
                            return Divider(
                            );
                          },
                        ),
                      );
                    },
                  ),
                      ],
                    ),
                  )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
